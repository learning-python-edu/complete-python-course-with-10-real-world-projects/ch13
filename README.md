# Chapter 13: Using Python with CSV, JSON, and Excel Files

## Setup virtual environment and install dependencies

This project dependencies are managed by _Poetry_, so in order to be able to use virtual environment and to install 
required dependencies [Poetry](https://python-poetry.org/docs/#installation) must be installed.

The easiest way to activate the virtual environment is to create a nested shell with following command:
```shell
poetry shell
```

To install the defined dependencies for your project, just run the following command:
```shell
poetry install
```

## Getting started with Jupyter

To start Jupyter execute following command:
```shell
poetry run jupyter notebook
```
