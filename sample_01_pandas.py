import pandas as pd

df1 = pd.DataFrame([[2, 4, 6], [10, 20, 30]])
print(df1)
print()

df1 = pd.DataFrame([[2, 4, 6], [10, 20, 30]], columns=['Price', 'Age', 'Value'])
print(df1)
print()

df1 = pd.DataFrame([[2, 4, 6], [10, 20, 30]], columns=['Price', 'Age', 'Value'], index=['First', 'Second'])
print(df1)
print()


df2 = pd.DataFrame([{'Name': 'John', 'Surname': 'Doe'}, {'Name': 'Jane'}])
print(df2)
print()

print(type(df1))
print()
print(df1.mean())
print()
print(type(df1.mean()))
print()
print(df1.mean().mean())

print(df1.Price)
print()
print(type(df1.Price))
print()
print(df1.Price.mean())
print(df1.Price.max())
